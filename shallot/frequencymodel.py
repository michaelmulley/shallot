#coding: utf-8

from collections import defaultdict
from heapq import nlargest
import itertools
from operator import itemgetter
import re

from . import settings

r_punctuation = re.compile(r"[^\s\w0-9'’—-]", re.UNICODE)
r_whitespace = re.compile(r'[\s—]+')

def text_token_iterator(text):
    text = r_punctuation.sub('', text.lower())
    for word in r_whitespace.split(text):
        if word:
            yield word

def line_token_iterator(lines, separate_lines=True):
    for line in lines:
        for word in text_token_iterator(line):
            yield word
        if separate_lines:
            yield '/'

def ngram_iterator(tokens, n=2):
    sub_iterators = itertools.tee(tokens, n)
    for i, iterator in enumerate(sub_iterators[1:]):
        for x in xrange(i + 1):
            # Advance the iterator i+1 times
            next(iterator, None)
    for words in itertools.izip(*sub_iterators):
        if '/' not in words:
            yield ' '.join(words)


class FrequencyModel(dict):
    """
    Given an iterable of strings, constructs an object mapping each string
    to the probability that a randomly chosen string will be it (that is, 
    # of occurences of string / # total number of items).
    """

    def __init__(self, items, min_count=1):
        counts = defaultdict(int)
        total_count = 0;
        for item in items:
            if len(item) > 2:
                counts[item] += 1
                total_count += 1
        self.count = total_count
        total_count = float(total_count)
        self.update(
            (k, v / total_count) for k, v in counts.iteritems() if v >= min_count
        )

    def __missing__(self, key):
        return float()

    def diff(self, other, diff_func=settings.DIFF_FUNC):
        """
        Given another FrequencyModel, returns a FrequencyDiffResult containing the difference
        between the probability of a given word appears in this FrequencyModel vs the other
        background model.
        """
        r = FrequencyDiffResult()
        min_prob = float(1) / other.count
        for k, v in self.iteritems():
            if k not in settings.STOPWORDS:
                r[k] = diff_func(self[k], other.get(k, min_prob))
        return r

    def most_common(self, n=None):    
        if n is None:
            return sorted(self.iteritems(), key=itemgetter(1), reverse=True)
        return nlargest(n, self.iteritems(), key=itemgetter(1))

    @classmethod
    def from_words(cls, words, ngram=1, min_count=1):
        if ngram > 1:
            words = ngram_iterator(words, ngram)
        return cls(words, min_count=min_count)

    @classmethod
    def from_lines(cls, lines, separate_lines=True, ngram=1, min_count=1):
        return cls.from_words(line_token_iterator(lines, separate_lines=separate_lines),
            ngram=ngram, min_count=min_count)

class FrequencyDiffResult(dict):

    def __init__(self, default=0.0):
        self.default = default

    def __missing__(self, key):
        return self.default

    def most_common(self, n=10):
        return nlargest(n, self.iteritems(), key=itemgetter(1))

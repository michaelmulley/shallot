import cPickle as pickle
import os
import re

from .frequencymodel import FrequencyModel

def _get_background_model_path(corpus_name, n, directory=None):
    # Sanitize corpus_name, since it might be user input
    corpus_name = re.sub(r'[^a-z0-9-]', '', corpus_name)
    if directory is None:
        directory = os.environ.get('SHALLOT_LANGUAGE_MODEL_PATH', None)
        if directory is None:
            directory = os.path.join(os.path.dirname(__file__), '..', 'language_model_data')
    # FIXME test dir existence
    return os.path.join(directory, '%s.%dgram' % (corpus_name, n))

def load_background_model(corpus_name, ngram, directory=None):
    with open(_get_background_model_path(corpus_name, ngram, directory), 'rb') as f:
        return pickle.load(f)

def generate_background_models(corpus_name, lines, ngram_lengths=[1,2,3]):
    for n in ngram_lengths:
        lines = list(lines)
        bg = FrequencyModel.from_lines(lines, ngram=n, min_count=5 if n < 3 else 3)
        with open(_get_background_model_path(corpus_name, n), 'wb') as f:
            pickle.dump(bg, f, pickle.HIGHEST_PROTOCOL)
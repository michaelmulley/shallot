import argparse
import codecs
import json

from .analyze import analyze_language
from .corpora import generate_background_models

def main():
	parser = argparse.ArgumentParser()
	subparsers = parser.add_subparsers()

	parser_bgmodel = subparsers.add_parser('genbg')
	parser_bgmodel.add_argument('corpus_name')
	parser_bgmodel.add_argument('files', nargs='*')
	parser_bgmodel.set_defaults(func=genbg)

	parser_analyze = subparsers.add_parser('analyze')
	parser_analyze.add_argument('corpus_name')
	parser_analyze.add_argument('file')
	parser_analyze.set_defaults(func=analyze_command)

	args = parser.parse_args()
	args.func(args)

def files_lines_iterator(filenames):
	for filename in filenames:
		with codecs.open(filename, encoding='utf8') as f:
			for line in f:
				yield line

def genbg(args):
	generate_background_models(args.corpus_name, files_lines_iterator(args.files))

def analyze_command(args):
	results = analyze_language(files_lines_iterator([args.file]), args.corpus_name)
	print json.dumps(results, indent=4)

if __name__ == '__main__':
	main()

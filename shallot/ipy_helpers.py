from glob import glob
import os
import random

import logging
logging.basicConfig()

def load_foreground(name=None):
    if name is None:
        # If no name is provided, pick someone at random.
        name = os.path.basename(random.choice(glob('./pol-corpora/*')))
        logging.info("Picking %s" % name)
    with open('./pol-corpora/%s' % name) as f:
        return f.readlines()
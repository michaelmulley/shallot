from .corpora import load_background_model
from .frequencymodel import FrequencyModel
from . import settings

def analyze_language(foreground_lines, background_corpus_name):
    """Return a list of ngrams that are more common in foreground_lines than
    in the background corpus.

    ("more common" is intentionally vague, the algorithm is in flux)

    foreground_lines -- an iterable of plain-text strings (sentences, paragraphs)
    background_corpus_name -- string name of the pre-built corpus, in the language model data dir
    """
    results = []

    seen = set(settings.STOPWORDS)
    if not isinstance(foreground_lines, list):
        foreground_lines = list(foreground_lines)
    for opts in settings.NGRAM_LENGTHS:
        bg = load_background_model(background_corpus_name, ngram=opts['length'])
        model = FrequencyModel.from_lines(foreground_lines, ngram=opts['length'], min_count=4)

        result = model.diff(bg)
        top = iter(result.most_common(50))
        count = 0
        for item in top:
            if count >= opts['max_count']:
                continue
            words = item[0].split(' ')
            #if sum(word in seen for word in words) / float(len(words)) < 0.6:
            if words[0] not in seen and words[-1] not in seen:
                seen.update(words)
                results.append({
                    "text": item[0],
                    "score": item[1] * 1000
                    #"size": _log_scale(item[1], opts['range'])
                })
                count += 1
    #results.sort(key=lambda r: r['size'], reverse=True)
    return results;